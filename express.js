const exp = require('express')
const app = exp();

const port = 2000;
const { ecb,apiCb } = require('./callback')
try {
    app.get('',apiCb);
    app.listen(port, ecb);
    console.log(`connected to port ${port}`);
} catch (error) {
    console.log('failed to connects');
}
